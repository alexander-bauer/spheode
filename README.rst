Spheode
=======

Spheode is an opinionated, open-source documentation tool built atop Sphinx.
Source documentation are typically living reStructuredText documentation
intermingled with code. From those sources, Spheode renders HTML documentation
using Sphinx, either repeatably on-the-fly during authorship, or during a CI
pipeline.

.. note::

   This page is best viewed at the `Spheode home page
   <https://alexander-bauer.gitlab.io/spheode/>`_, rather than in an RST viewer
   on GitLab.

Usage
-----

.. highlight:: bash

Spheode has two modes: ``autobuild`` and ``export``. When invoked without an
argument, ``autobuild`` is the default mode. In both modes, the *root* of the
source repository must be mounted to ``/repo``.

``autobuild``
^^^^^^^^^^^^^

In ``autobuild`` mode, Spheode invokes ``sphinx-autobuild``, which
automatically rebuilds the HTML documentation on changes to the source
directory, and serves it on port 8000 in the container. This is the typical
mode for developers to use while authoring documentation. ::

   $ docker run -v "$(pwd):/repo:ro" -p "8000:8000" registry.gitlab.com/alexander-bauer/spheode:latest

An easy way to use this is to set an alias in your ``.bashrc``. ::

   alias spheode='docker run -v "$(pwd):/repo:ro" -p "8000:8000" registry.gitlab.com/alexander-bauer/spheode:latest'

``export``
^^^^^^^^^^

In ``export`` mode, Spheode invokes ``sphinx-build`` once and delivers the
resulting HTML documentation to the volume mounted at ``/export``. This might
be used in a CI context to build static documentation for new versions of the
source, such as for GitHub or GitLab Pages. ::

   $ docker run -v "$(pwd):/repo:ro" -v "$(pwd)/export:/export" registry.gitlab.com/alexander-bauer/spheode:latest export

Additional arguments, such as to ``chown`` the export to another user, may be
passed just by appending them. ::

   $ docker run -v "$(pwd):/repo:ro" -v "$(pwd)/export:/export" registry.gitlab.com/alexander-bauer/spheode:latest export --chown 1000:1000

``check``
^^^^^^^^^^

In ``check`` mode, Spheode invokes ``sphinx-build`` with arguments to nit-pick
and treat warnings as errors, suppresses most other output, and passes through
its return code. This mode does not save any build artifacts (although it does
perform an ``html`` build.) The purpose is to vet the current repository to
ensure there are no errors or warnings in the documentation build. This is also
suitable for use in CI, or with :ref:`pre-commit hooks <index:using spheode in
ci>`. ::

   $ docker run -v "$(pwd):/repo:ro" registry.gitlab.com/alexander-bauer/spheode:latest check

Using Spheode in CI
^^^^^^^^^^^^^^^^^^^

Spheode is distributed as a container with CI in mind as a first-class consumer.

GitLab CI
"""""""""

For GitLab CI publishing to GitLab Pages (for both GitLab.com and self-hosted
GitLab), all that is necessary is for the consuming project to add a ``pages``
job to their ``.gitlab-ci.yml`` like so.

.. literalinclude:: gitlab-ci.yml
   :caption: .gitlab-ci.yml

Using Spheode with pre-commit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`pre-commit <https://pre-commit.com/>`_ is a framework for sharing and using
Git pre-commit hooks. Spheode provides a hook for invoking check mode, which
will trigger on changes to the ``docs/`` folder or any ``.rst`` files. For
complex include trees (such as source code included via ``..
include-comment::``) or non-RST content, it may be desirable to override the
``files`` option.

To use the hook, add a declaration like this to your ``.pre-commit-config.yaml``.

.. code-block:: yaml

  - repo: https://gitlab.com/alexander-bauer/spheode
    rev: v0.10
    hooks:
      - id: spheode-check

Debugging
^^^^^^^^^

Entering the Container Interactively
""""""""""""""""""""""""""""""""""""

Typically Spheode should not be entered interactively, but for debugging
purposes, it may be entered by overriding the container ``entrypoint``, like ::

   $ docker run -v $(pwd):/repo --entrypoint sh -it registry.gitlab.com/alexander-bauer/spheode:latest

Details
-------

Sources
^^^^^^^

Documentation sources are reStructuredText files in the ``docs`` subdirectory
of the root of the repository. The root of the documentation is ``index.rst``.
Optionally, ``conf.py`` can be provided to set Sphinx configuration options;
see `index:Sphinx Configuration`. ::

  .                   <-- repository root
  └── docs            <-- documentation root
      ├── conf.py     <-- optional user-provided configuration
      └── index.rst   <-- documentation index or entrypoint


In addition to regular reStructuredText sources, Spheode includes the
``sphinxcontrib-cmtinc`` plugin, which adds the capability to ingest
reStructuredText from comments in source code.

Sphinx Configuration
""""""""""""""""""""

Optionally, ``conf.py`` can be provided in the root of the ``docs`` directory in
order to set Sphinx configuration options. Many options are set automatically
by Spheode, but project-specific options (such as ``project``, ``author``, and
``copyright``) may be set. Spheode provides some functions for convenience,
such as ``copyright_since(author, start_year, end_year=current_year)``

For example, here is the ``conf.py`` included with this project.

.. literalinclude:: conf.py
   :caption: conf.py
   :language: python

Prolog / Epilog
"""""""""""""""

Files named ``prolog.rst`` or ``epilog.rst`` in the root of the ``docs``
directory are set as ``rst_prolog`` or ``rst_epilog`` in the Sphinx
configuration.  Those configuration options do not need to be set. To extend
the content of either programatically, simply override ``rst_prolog`` or
``rst_epilog`` in the custom ``conf.py``. The content as set by Spheode is
available as ``default_rst_prolog`` and ``default_rst_epilog``.

Documentation in Source Code
""""""""""""""""""""""""""""

Spheode includes the ``sphinxcontrib-cmtinc`` plugin, which provides a
directive to include Sphinx documentation embedded within other files, such as
source code. For example, here is source code and a directive for including it.

.. tab:: Source

   .. code-block:: RST
      :caption: Sphinx directive

      .. include-comment:: example.py
         :style: hash

   .. literalinclude:: example.py
      :caption: example.py
      :language: python

.. tab:: Result

   .. include-comment:: example.py
      :style: hash

Another less contrived example of this feature in action is for documenting
Ansible playbooks and roles.

.. tab:: Source

   .. code-block:: RST
      :caption: Sphinx directive

      .. include-comment:: example-playbook.yml
         :style: hash

   .. literalinclude:: example-playbook.yml
      :caption: example-playbook.yml
      :language: yaml+jinja

.. tab:: Result

   .. include-comment:: example-playbook.yml
      :style: hash

Graphviz
""""""""

Spheode includes the Graphviz CLI tooling as well as the `Sphinx extension`__.
This makes it easy to include Graphviz graphs, either from external files or
directly in RST. (This is also supported inside comments included by
``cmtinc``.)

.. __: https://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html

.. tab:: Source

   .. code-block:: RST
      :caption: Sphinx directive

      .. graphviz::

         digraph foo {
            "bar" -> "baz";
         }

.. tab:: Result

   .. graphviz::

      digraph foo {
         bar -> baz;
      }

Mermaid
"""""""

Mermaid is another graphing library with its full toolkit available in Spheode.
Because the ``mmdc`` CLI tool is available, the output format is set to SVG for
compatibility with the non-HTML output.

.. code-block:: RST
   :caption: Sphinx directive

   .. mermaid::

      flowchart LR
         id1 -.-> id2

.. mermaid::

   flowchart LR
      id1 -.-> id2


Sphinx Design
"""""""""""""

Spheode includes the Sphinx-Design `extension`__, which adds a number of
graphical elements.

.. __: https://sphinx-design.readthedocs.io/

Sphinx Toolbox
""""""""""""""

Spheode includes the Sphinx Toolbox `extension`__ collection. By default, only
the collapse extension is enabled. This allows for dropdown elements (including
in Confluence, due to support between the Confluence Builder and Sphinx
Toolbox) such as may be useful for literate programming. For example:

.. __: https://sphinx-toolbox.readthedocs.io/en/latest/

.. tab:: Source

   .. code-block:: RST
      :caption: Sphinx directive

      This example paragraph makes reference to a code block, which is folded by default.

      .. collapse:: Source

         .. code-block:: python

            def hello_world():
                return "Hello World"

            hello_world()

.. tab:: Result

   This example paragraph makes reference to a code block, which is folded by default.

   .. collapse:: Source

      .. code-block:: python

         def hello_world():
             return "Hello World"

            hello_world()

Convenience Functions
"""""""""""""""""""""

These functions are available in the local namespace of the custom ``conf.py``.
They do not need to be imported to be used.

.. highlight:: python

``copyright_since``
  This function, given an author and year, produces copyright statements in the form expected by Sphinx. ::

     >>> copyright_since("John Doe", "2002")
     "2002-2022, John doe"

     >>> copyright_since("John Doe", "2002", "2010")
     "2002-2020, John doe"

     >>> copyright_since("John Doe", "2022")
     "2022, John doe"

Internals
---------

Spheode is a container. It may be built and run using Docker or Podman. Its
entrypoint is the script ``spheode``, which invokes Sphinx in a structured way.

Configuration
^^^^^^^^^^^^^

Spheode provides its own ``conf.py`` for use by Sphinx which sets many default
options. These can be extended by a ``conf.py`` in the user ``docs`` source.
Mechanically, Spheode assembles a temporary ``conf`` directory and supplies it
to Sphinx using the ``-c`` option. This directory contains a ``conf.py``, which
is the entrypoint to the Sphinx configuration. This file reads and then runs
``exec()`` on ``spheode.py``, which sets default options, and then
``userconf.py``, which is a symlink to the user-provided ``conf.py``.
