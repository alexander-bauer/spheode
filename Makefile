CONTAINER := registry.gitlab.com/alexander-bauer/spheode
VERSION := $(shell git describe --tags)

build:
	buildah build \
		--tag "$(CONTAINER):latest" \
		--tag "$(CONTAINER):$(VERSION)" \
		--build-arg "VERSION=$(VERSION)" \
		src

# Publish
publish: publish-version publish-latest

publish-version:
	buildah push "docker://$(CONTAINER):$(VERSION)"

publish-latest:
	buildah push "docker://$(CONTAINER):latest"

# Release
release: build publish
